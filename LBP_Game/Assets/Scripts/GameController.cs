﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class GameController : MonoBehaviour {

    public GameObject [] obstacle;//up/down/Sleft/Sright/Rleft/Rright
    public Vector3 [] spawnValues;

    public GameObject scorePrefab;
    public Vector3[] scoreValues;

    public GameObject leftArraw;
    public GameObject rightArrow;

    public int obstacleCount;
    public float spawnWait;
    public float startWait;

    public float waveWait;
    private int random;
    public GameObject player;
    bool gameStart;
    bool spawn;

    bool flexsion;
    bool extension;
    bool lateralTilt;

    int[] exercise;
    int numOfExercises = 0;
    int[] randomNumbers;
    int repetition;
    int score;
    PlayerController playerControl;
    GameObject pc;
    bool playerWin;

    public Image life1;
    public Image life2;
    public Image life3;
    int numOfLife = 3;

    bool mainMenu;
    public Image mainMenuimg;


    void Start()
    {

        StartCoroutine(LoadDevice("cardboard"));

        gameStart = false;
        spawn = true;
        repetition = PlayerPrefs.GetInt("Repetition");

        GenerateRandomObstacles();
        obstacleCount = randomNumbers.Length;
        mainMenu = false;
        
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (String.Compare(XRSettings.loadedDeviceName, newDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(newDevice);
            yield return null;
            XRSettings.enabled = true;
        }
    }

    private void GenerateRandomObstacles()
    {
        FillExerciseList();

        randomNumbers = new int[repetition * exercise.Length];
        int index = 0;

        for (int i = 0; i < exercise.Length; i++)
        {

            for (int j = 0; j < 3; j++)
            {

                randomNumbers[index] = exercise[i];
                index++;
            }

        }

        for (int i = 3; i < repetition; i++)
        {

            for (int j = 0; j < exercise.Length; j++)
            {

                randomNumbers[index] = exercise[j];
                index++;
            }

            //Shufle Exercise 
            Shuffle(exercise);

        }

    }

    void Shuffle(int[] list)
    {

        int n = list.Length;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            int value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    private void FillExerciseList()
    {
        List<int> exerList = new List<int> ();

        if (Convert.ToBoolean(PlayerPrefs.GetString("Extension"))) {
            numOfExercises++;
            exerList.Add(0);
        }

        if (Convert.ToBoolean(PlayerPrefs.GetString("Flexsion")))
        {
            numOfExercises++;
            exerList.Add(1);
        }

        if (Convert.ToBoolean(PlayerPrefs.GetString("LateralTilt")))
        {
            numOfExercises++;
            exerList.Add(2);
            exerList.Add(3);
        }

        if (Convert.ToBoolean(PlayerPrefs.GetString("Rotation")))
        {
            numOfExercises++;
            exerList.Add(4);
            exerList.Add(5);
        }

        exercise = exerList.ToArray();
        //PlayerPrefs.SetInt("Repetition", 5);
    }

    private void Update()
    {
        if (Input.anyKey) gameStart = true;

        if (gameStart && spawn) {

            StartSpawn();
        }



    }

    IEnumerator SpawnObstacles()
    {
        yield return new WaitForSeconds(startWait);
        //while (true)
        //{
        

        for (int i = 0; i < obstacleCount; i++)
            {
                random = randomNumbers[i];
                Vector3 spawnPosition = new Vector3(spawnValues[random].x, spawnValues[random].y, spawnValues[random].z + player.transform.position.z);
                Quaternion spawnRotation = Quaternion.identity;
            

            if (obstacle[random] != null) {

                    Instantiate(obstacle[random], spawnPosition, spawnRotation);
                    Instantiate(scorePrefab, new Vector3(scoreValues[random].x, scoreValues[random].y, scoreValues[random].z + player.transform.position.z), spawnRotation);
                    
                    if (random == 2 || random ==3 )
                    {
                        Instantiate(obstacle[random], new Vector3(0, 0, spawnValues[random].z + player.transform.position.z), spawnRotation);
                        Instantiate(scorePrefab, new Vector3(scoreValues[random].x, scoreValues[random].y, scoreValues[random].z + player.transform.position.z), spawnRotation);

                    }

                    if (random == 4)
                    {
                        Instantiate(obstacle[random], new Vector3(spawnValues[5].x, spawnValues[5].y, spawnValues[5].z + player.transform.position.z), spawnRotation);

                        Instantiate(obstacle[random], new Vector3(1.22f , spawnValues[5].y, spawnValues[5].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(2f, spawnValues[5].y, spawnValues[5].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(-1.22f, spawnValues[5].y, spawnValues[5].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(-2f, spawnValues[5].y, spawnValues[5].z + player.transform.position.z), spawnRotation);

                        Instantiate(leftArraw, new Vector3(-0.03f, 1.5f, scoreValues[random].z + player.transform.position.z + 1), spawnRotation);
                    }

                    if(random == 5)
                    {
                        Instantiate(obstacle[random], new Vector3(spawnValues[4].x, spawnValues[4].y, spawnValues[4].z + player.transform.position.z), spawnRotation);

                        Instantiate(obstacle[random], new Vector3(1.22f, spawnValues[4].y, spawnValues[4].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(2f, spawnValues[4].y, spawnValues[4].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(-1.22f, spawnValues[4].y, spawnValues[4].z + player.transform.position.z), spawnRotation);
                        Instantiate(obstacle[random], new Vector3(-2f, spawnValues[4].y, spawnValues[4].z + player.transform.position.z), spawnRotation);


                        Instantiate(rightArrow, new Vector3(-0.03f, 1.5f, scoreValues[random].z + player.transform.position.z + 1), spawnRotation);
                        
                    }
                }

               

                yield return new WaitForSeconds(spawnWait);
            }
        
            yield return new WaitForSeconds(waveWait);
        //}

            GameWin();
       
    }

    public void GameOver() {

        if (numOfLife == 0)
        {
            Debug.Log("Game Over");
            StopCoroutine(SpawnObstacles());
            SceneManager.LoadScene("GameOver");
            SceneManager.UnloadSceneAsync("Game");
        }
        else if (numOfLife == 1)
        {
            numOfLife--;
            life1.enabled = false;
        }
        else if (numOfLife == 2)
        {
            numOfLife--;
            life2.enabled = false;
        }
        else if (numOfLife == 3)
        {
            numOfLife--;
            life3.enabled = false;
        }

    }

    public void GameWin() {

        StopCoroutine(SpawnObstacles());
        SceneManager.LoadScene("GameWin");
        SceneManager.UnloadSceneAsync("Game");

    }

    public bool getGameStart() {

        return gameStart;
    }

    public void setGameStart(bool gameState) {

        gameStart = gameState; 
    }

    public void StartSpawn() {

        StartCoroutine(SpawnObstacles());
        spawn = false;
    }


    public void ExitGame()
    {

       SceneManager.LoadScene("MainMenu");

    }


}
