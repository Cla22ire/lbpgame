﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

public class MainMenu : MonoBehaviour {

    public Text PlayerLevel;
    public Text PlayerScore;

    public Canvas settingCanvas;
    public Canvas reportCanvas;
    public Canvas profileCanvas;
    public Canvas popupMsg;

    public Canvas infoMsgCanvase;
    public CanvasGroup infoMsgCanvasGroup;
    public Text infomsgText;

    public Toggle flexsion;
    public Toggle extension;
    public Toggle lateralTilt;
    public Toggle rotation;

    public Slider repetition;
    public Slider frequency;
    public Slider days;

    public Dropdown levelList;
    public GameObject SettingsMsg;

    public InputField repetitionValue;
    public InputField frequencyValue;
    public InputField daysValue;

    public Scrollbar scrolbar;
    bool FillBar;

    bool fade;
    //float duration;
    public Text email;
    public Text username;
    public Text password;
    public Text ErroMsg;

    public Text showemail;
    public Text showusername;
    public Text showpassword;

    public Text user;

    // Use this for initialization
    void Start() {

        //UnityEngine.XR.XRSettings.enabled = false;

        PlayerLevel.text = (PlayerPrefs.GetInt("Level") + 1) + "";
        PlayerScore.text = PlayerPrefs.GetInt("CurrentScore") + " / " + PlayerPrefs.GetInt("TargetScore");

        scrolbar.size = 0f;
        FillBar = true;
        fade = false;

        showemail.text = PlayerPrefs.GetString("Email");
        showusername.text = PlayerPrefs.GetString("Username");
        showpassword.text = PlayerPrefs.GetString("Password");
        user.text = PlayerPrefs.GetString("Username");
    }

    private void Update()
    {
        int currentScore = PlayerPrefs.GetInt("CurrentScore");
        int targetScore = PlayerPrefs.GetInt("TargetScore");

        float fill = (currentScore * 1.0f / targetScore * 1.0f);

        if (FillBar) {
            scrolbar.size += 0.1f * Time.deltaTime * 2;
            if (scrolbar.size >= fill)
                FillBar = false;
        }

        if (fade) {

            infoMsgCanvasGroup.alpha -= 0.1f * Time.deltaTime * 5;

            if (infoMsgCanvasGroup.alpha <= 0) {
                fade = false;
                infoMsgCanvase.enabled = false;
            }
        }
    }

    public void StartGame() {


        SceneManager.LoadScene("BeforeGame");


    }

    public void OpenSettingsW() {

        settingCanvas.enabled = true;

        if (PlayerPrefs.HasKey("Flexsion")) {

            flexsion.isOn = Convert.ToBoolean(PlayerPrefs.GetString("Flexsion"));
            extension.isOn = Convert.ToBoolean(PlayerPrefs.GetString("Extension"));
            lateralTilt.isOn = Convert.ToBoolean(PlayerPrefs.GetString("LateralTilt"));
            rotation.isOn = Convert.ToBoolean(PlayerPrefs.GetString("Rotation")); 

            repetition.value = PlayerPrefs.GetInt("Repetition");
            repetitionValue.text = PlayerPrefs.GetInt("Repetition") + "";

            frequency.value = PlayerPrefs.GetInt("Frequency");
            frequencyValue.text = PlayerPrefs.GetInt("Frequency") + "";

            days.value = PlayerPrefs.GetInt("Days");
            daysValue.text = PlayerPrefs.GetInt("Days") + "";

            levelList.value = PlayerPrefs.GetInt("Level");

        }

    }

    public void CloseSettingsW()
    {

        settingCanvas.enabled = false;
        //FerroMessage.GetComponent<Text>().enabled = false;
        //EmailSentMessage.GetComponent<Text>().enabled = false;
    }

    public void OpenReportW() { reportCanvas.enabled = true; }

    public void CloseReportW() { reportCanvas.enabled = false; }

    public void OpenProfileW()
    {

        profileCanvas.enabled = true;

    }

    public void CloseProfileW()
    {

        profileCanvas.enabled = false;
    }

    public void SaveProfileInfo() {

        if (!email.text.Equals(""))
        {
            //show error message
            //ErroMsg.enabled = true;
            //return;

            PlayerPrefs.SetString("Email", email.text);
        }

        if (!username.text.Equals(""))
        {
            //show error message
            //ErroMsg.enabled = true;
            //return;

            PlayerPrefs.SetString("Username", username.text);
        }

        if (!password.text.Equals(""))
        {
            //show error message
            //ErroMsg.enabled = true;
            //return;

            PlayerPrefs.SetString("Password", password.text);
        }


        user.text = PlayerPrefs.GetString("Username");
        CloseProfileW();
        ErroMsg.enabled = false;
        StartCoroutine(ShowInfoMsg("!ﺡﺎﺠﻨﺑ ﻆﻔﺤﻟﺍ ﻢﺗ"));
        showemail.text = PlayerPrefs.GetString("Email");
        showusername.text = PlayerPrefs.GetString("Username");
        showpassword.text = PlayerPrefs.GetString("Password");
    }


    public void UpdateSettings() {
        //must select at least one exercise 

        if (!flexsion.isOn && !lateralTilt.isOn && !extension.isOn && !rotation.isOn)
        {
            SettingsMsg.GetComponent<Text>().enabled = true;
            return;
        }

        PlayerPrefs.SetString("Flexsion", flexsion.isOn + "");
        PlayerPrefs.SetString("Extension", extension.isOn + "");
        PlayerPrefs.SetString("LateralTilt", lateralTilt.isOn + "");
        PlayerPrefs.SetString("Rotation", rotation.isOn + "");

        PlayerPrefs.SetInt("Repetition", (int) repetition.value);
        PlayerPrefs.SetInt("Frequency", (int) frequency.value);
        PlayerPrefs.SetInt("Days", (int) days.value);

        PlayerPrefs.SetInt("Level", levelList.value);

        PlayerPrefs.SetInt("TargetScore", (int)repetition.value * (int)frequency.value * (int)days.value);
        PlayerPrefs.SetInt("CurrentScore", 0);


        Start();
        SettingsMsg.GetComponent<Text>().enabled = false;
        CloseSettingsW();
        StartCoroutine(ShowInfoMsg("!ﺡﺎﺠﻨﺑ ﺕﺍﺩﺍﺪﻋﻹﺍ ﻆﻔﺣ ﻢﺗ"));

    }

    public void SlidingValues() {

        repetitionValue.text = repetition.value +"";

        frequencyValue.text = frequency.value + "";

        daysValue.text = days.value + "";

    }

    public void OpenPopupMsgtW() {

        popupMsg.enabled = true;

    }

    public void ClosePopupMsgW() {

        popupMsg.enabled = false;
    }

    public void SenReportEmail() {

        //close the window + show confirmation message 
        ClosePopupMsgW();
        StartCoroutine(ShowInfoMsg("!ﺡﺎﺠﻨﺑ ﻝﺎﺳﺭﻹﺍ ﻢﺗ"));
    }

    public void ShowResetMsg() {

        StartCoroutine(ShowInfoMsg("!ﺕﺎﻧﺎﻴﺒﻟﺍ ﻊﻴﻤﺟ ﻑﺬﺣ ﻢﺗ"));
    }

    IEnumerator ShowInfoMsg(string msg)
    {
        infoMsgCanvase.enabled = true;
        infomsgText.text = msg;
        infoMsgCanvasGroup.alpha = 1f;

        yield return new WaitForSeconds(1);
        fade = true;
        StopCoroutine(ShowInfoMsg(""));
    }

    public void Logout()
    {
        PlayerPrefs.SetString("RememberMe", "false");
        SceneManager.LoadScene("Login");        
    }
}
