﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Net.Mail;

[System.Serializable]

public class RecordItem {

    public string date;
    public int Tfrequency;
    public int days;
    public int repetition;
    public int level; 

    public bool flexsion;
    public bool lateralTilt;
    public bool extension;
    public bool rotation;

    public int count;
    

    public RecordItem(string d, int freq, int numDays, int rep, int lvl, bool f, bool e, bool l, bool r, int c) {

        date = d;

        Tfrequency = freq;
        days = numDays;
        repetition = rep;
        level = lvl;

        flexsion = f;
        extension = e;
        lateralTilt = l;
        rotation = r;

        count = c;       

    }
}

public class CreateRecordList : MonoBehaviour {

    public List<RecordItem> recordlist;
    public Transform contentPanel;
    public GameObject recordPrefab;
    List<RecordItem> list;
    public Canvas reportCanvas;

    // Use this for initialization
    void Start () {

       ReadFromFile();
       CalculateFrequency();
       PopulateList();
        
    }

    private void ReadFromFile()
    {

        //StreamReader reader = new StreamReader(@"HistoryReport.txt");

        StreamReader reader = new StreamReader(Application.persistentDataPath + "HistoryReport.txt");

        string line = reader.ReadLine();

        while (line != null) {

            string[] fields = line.Split(':');
            Debug.Log(fields.Length+ "--------------"+ fields[0]+ fields[1]+ fields[2]+ fields[3]);
            recordlist.Add(new RecordItem(fields[0], Convert.ToInt32(fields[1]), Convert.ToInt32(fields[2]), Convert.ToInt32(fields[3]),
                Convert.ToInt32(fields[4]), Convert.ToBoolean(fields[5]), Convert.ToBoolean(fields[6]), Convert.ToBoolean(fields[7]), Convert.ToBoolean(fields[8]), 0));
            line = reader.ReadLine();
        }

        reader.Close();

        
    }

    void CalculateFrequency()
    {
        list = new List<RecordItem>();
        int index = 0;

        string currentDate;
        bool currentFlexsion, currentExtension, currentRotation, currentLateral;
        int currentDays, currentReptirion, currentFreq, currentLevel;

        string nextDate;
        bool nextFlexsion, nextExtension, nextRotation, nextLateral;
        int nextDays, nextReptirion, nextFreq, nextLlevel;

        int count = 1;


        if (recordlist.Count == 1) {
            recordlist[index].count = count;
            list.Add(recordlist[index]);
            return;
        }

        while (true) {


            RecordItem currentRecord = recordlist[index];
            currentDate = currentRecord.date;
            currentDays = currentRecord.days;
            currentReptirion = currentRecord.repetition;
            currentFreq = currentRecord.Tfrequency;
            currentFlexsion = currentRecord.flexsion;
            currentExtension = currentRecord.extension;
            currentRotation = currentRecord.rotation;
            currentLateral = currentRecord.lateralTilt;
            currentLevel = currentRecord.level;
            //Debug.Log("Current: "+currentDate);

            RecordItem nextRecord = recordlist[index + 1];
            nextDate = nextRecord.date;
            nextDays = nextRecord.days;
            nextReptirion = nextRecord.repetition;
            nextFreq = nextRecord.Tfrequency;
            nextFlexsion = nextRecord.flexsion;
            nextExtension = nextRecord.extension;
            nextRotation = nextRecord.rotation;
            nextLateral = nextRecord.lateralTilt;
            nextLlevel = nextRecord.level;
            //Debug.Log("Next: "+  nextDate);


            if (currentDate.Equals(nextDate) && currentDays.Equals(nextDays) && currentReptirion.Equals(nextReptirion) && currentFreq.Equals(nextFreq) &&
                currentFlexsion.Equals(nextFlexsion) && currentExtension.Equals(nextExtension) && currentRotation.Equals(nextRotation) && 
                currentLateral.Equals(nextLateral) && currentLevel.Equals(nextLlevel))
            {
                count++;
                Debug.Log("In the if ");
                if (index == (recordlist.Count - 2)) {
                    Debug.Log("In the nested if if");
                    recordlist[index].count = count;
                    list.Add(recordlist[index]);
                    return;
                }
            }
            else
            {
                recordlist[index].count = count;
                list.Add(recordlist[index]);
                Debug.Log("In the else ");
                count = 1;

            }

            if (index == (recordlist.Count - 2))
            {
                Debug.Log("In the last if");
                recordlist[index + 1].count = count;
                list.Add(recordlist[index+1]);
                return;
            }

            index++;

            //list = recordlist.RemoveAll();
        }

        
        

    }

    void PopulateList()
    {
        foreach (var record in list) {
            
            //while true (currect date = previous date) count ++;
            GameObject newRecord = Instantiate(recordPrefab) as GameObject;
            RecordScript recordScript = newRecord.GetComponent<RecordScript>();
            //recordlist.FindAll();

            recordScript.date.text = record.date;
            Debug.Log(""+record.date);
            recordScript.frequency.text = record.Tfrequency + " / " + record.count + "";
            recordScript.programExer.text = ProgExercises(record.flexsion, record.extension, record.lateralTilt, record.rotation);

            recordScript.days.text = record.repetition + "";
            recordScript.repetition.text = record.days + "";
            recordScript.level.text = record.level + 1 + "";

            float per = record.count * 1f / record.Tfrequency * 1f;
            recordScript.freqPer.text = "" + (int) (per * 100) + " %";
            recordScript.freqFill.fillAmount = per;

            newRecord.transform.SetParent(contentPanel, false);
        }
    }

    private string ProgExercises(bool f, bool e, bool l, bool r)
    {
        string exercises = "";

        if (f && e && l)
            exercises = "ﻲﺒﻧﺎﺠﻟﺍ ﻞﻴﻤﻟﺍ ،ﻒﻠﺨﻠﻟ ﺀﺎﻨﺤﻧﻹﺍ ،ﻡﺎﻣﻸﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (f && e)
            exercises = "ﻒﻠﺨﻠﻟ ﺀﺎﻨﺤﻧﻹﺍ ،ﻡﺎﻣﻸﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (e && l)
            exercises = "ﻲﺒﻧﺎﺠﻟﺍ ﻞﻴﻤﻟﺍ ،ﻒﻠﺨﻠﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (f && l)
            exercises = "ﻲﺒﻧﺎﺠﻟﺍ ﻞﻴﻤﻟﺍ ،ﻡﺎﻣﻸﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (f)
            exercises = "ﻡﺎﻣﻸﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (e)
            exercises = "ﻒﻠﺨﻠﻟ ﺀﺎﻨﺤﻧﻹﺍ";
        else if (l)
            exercises = "ﻲﺒﻧﺎﺠﻟﺍ ﻞﻴﻤﻟﺍ";

        if (r)
            exercises += "ﻥﺍﺭﻭﺪﻟﺍ ،";

        if (!f && !e && !l && r)
            exercises = "ﻥﺍﺭﻭﺪﻟﺍ";


        return exercises;
    }

    public void SendEmail() {

        string message= "";

        foreach (var record in list)
        {

            //while true (currect date = previous date) count ++;
            string date = record.date;
            string frequency = record.Tfrequency + " / " + record.count + "";
            string programExer = ProgExercisesMail(record.flexsion, record.extension, record.lateralTilt, record.rotation);

            string days = record.days + "";
            string repetition = record.repetition + "";
            string level = record.level + 1 + "";

            message += String.Format("{0} :التاريخ" + "\n" + "{1} :عدد مرات تكرار التمرين باليوم" + "\n" + "{2} :مجموعة التمارين" + "\n" +
                "{4} :تكرار التمرين" + "\n" + "{3} :عدد الأيام" + "\n" + "{5} :المستوى" + "\n---------------------------------\n", 
                date, frequency, programExer, days, repetition, level);

        }

        SendMail(PlayerPrefs.GetString("Email"), message);      
        Debug.Log("Message has been sent");
        //PlayerPrefs.GetString("Email")
    }

    void SendMail(string email, string msg)
    {

        MailMessage mail = new MailMessage();
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("LBPExerGame@gmail.com");
        mail.To.Add(email);
        mail.Subject = "Exercise History Report";
        mail.Body = "\n\n" + msg;

        smtpServer.Port = 587;
        smtpServer.Credentials = new NetworkCredential("LBPExerGame@gmail.com", "lbpgame123") as System.Net.ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };


        smtpServer.Send(mail);
    }

    private string ProgExercisesMail(bool f, bool e, bool l, bool r)
    {
        string exercises = "";

        if (f && e && l)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻸﻣﺎﻡ، ﺍﻹﻧﺤﻨﺎﺀ ﻟﻠﺨﻠﻒ، ﺍﻟﻤﻴﻞ ﺍﻟﺠﺎﻧﺒﻲ";
        else if (f && e)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻸﻣﺎﻡ، ﺍﻹﻧﺤﻨﺎﺀ ﻟﻠﺨﻠﻒ";
        else if (e && l)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻠﺨﻠﻒ، ﺍﻟﻤﻴﻞ ﺍﻟﺠﺎﻧﺒﻲ";
        else if (f && l)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻸﻣﺎﻡ، ﺍﻟﻤﻴﻞ ﺍﻟﺠﺎﻧﺒﻲ";
        else if (f)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻸﻣﺎﻡ";
        else if (e)
            exercises = "ﺍﻹﻧﺤﻨﺎﺀ ﻟﻠﺨﻠﻒ";
        else if (l)
            exercises = "ﺍﻟﻤﻴﻞ ﺍﻟﺠﺎﻧﺒﻲ";

        if (r)
            exercises += "، ﺍﻟﺪﻭﺭﺍﻥ";

        if (!f && !e && !l && r)
            exercises = "ﺍﻟﺪﻭﺭﺍﻥ";

        return exercises;
    }

    public void ResetReport() {

        WriteOnFile();
        reportCanvas.enabled = false;
    }

    public void WriteOnFile() {

        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "HistoryReport.txt");
        writer.Write("");
        writer.Close();

        var children = new List<GameObject>();
        foreach (Transform child in contentPanel) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

}
