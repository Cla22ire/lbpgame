﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameOver : MonoBehaviour {


    public Image cooldown;
    public bool coolingDown;

    public Image mainMenuimg;
    public bool mainMenu;
    //public float waitTime = 30.0f;
    //public Camera GVrCamera;
    //public Vector3 GVRPosition;


    // Use this for initialization
    void Start () {

        StartCoroutine(LoadDevice("cardboard"));

        coolingDown = false;
        mainMenu = false;
        
        //Instantiate(GVrCamera, new Vector3(0,0,0) , Quaternion.identity);
        //cooldown.
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (String.Compare(XRSettings.loadedDeviceName, newDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(newDevice);
            yield return null;
            XRSettings.enabled = true;
        }
    }

    // Update is called once per frame
    void Update () {

        if (coolingDown == true)
        {
            //Reduce fill amount over 30 seconds
            cooldown.fillAmount = cooldown.fillAmount + ( 0.2f * Time.deltaTime);
            if (cooldown.fillAmount == 1) {
                SceneManager.LoadScene("BeforeGame");
                Debug.Log("Now you can load the game");
            }
        }

        if (mainMenu == true)
        {
            //Reduce fill amount over 30 seconds
            mainMenuimg.fillAmount = mainMenuimg.fillAmount + (0.2f * Time.deltaTime);
            if (mainMenuimg.fillAmount == 1)
            {
                SceneManager.LoadScene("MainMenu");
                Debug.Log("Now you can load the MainMenu");
            }
        }

    }

    public void PointerEnter() {

        coolingDown = true;

    }

    public void PointerExit()
    {

        coolingDown = false;
        cooldown.fillAmount = 0f;

    }

    public void PointerEnterMainMenu()
    {

        mainMenu = true;

    }

    public void PointerExitMainMenu()
    {

        mainMenu = false;
        mainMenuimg.fillAmount = 0f;

    }
}
