﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.WSA;

public class PlayerController : MonoBehaviour {

    Animator anim;
    Animator playeranim;
    public GameObject cam;
    private GameController gameController;
    BoxCollider boxCollider;

    static int s_JumpingSpeedHash = Animator.StringToHash("JumpSpeed");
    public float laneChangeSpeed = 1.0f;
    public int maxLife = 3;

    public Text scoreUI;
    public int GameScore = 0;

    public bool isJumping { get { return m_Jumping; } }
    public bool isSliding { get { return m_Sliding; } }


    protected bool m_Jumping;
    protected bool m_Sliding;

    protected int m_CurrentLane = k_StartingLane;
    protected Vector3 m_TargetPosition = Vector3.zero;

    protected readonly Vector3 k_StartingPosition = Vector3.forward * 2f;
    protected const int k_StartingLane = 1;

    private Gyroscope gyro;
    private bool gyroEnabled;
    float x, y, z, w;
    float roll, pitch, yaw, absoluteZ;

    bool left = false, right = false, leftRotation = false, rightRotation = false;

    float lerpTime = 5;
    //float currentLerpTime = 0;

    int[] flexsion = { -40 , -55, -70};
    int[] extension = { 5, 10, 15};
    int[] lateralTilt = { 20, 30, 40};
    int[] rotation = {15, 20, 25};
    int level;

    int numOfLife = 3;

    public AudioSource hit;
    public AudioSource coin;
    bool getAbsulutez;

    bool howToPlay;

    void Start()
    {

        level = PlayerPrefs.GetInt("Level");
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();

        playeranim = cam.GetComponent<Animator>();


        Scene scene = SceneManager.GetActiveScene();
        Debug.Log("Active scene is '" + scene.name + "'.");
        if (scene.name.Equals("HowToPlay"))
            howToPlay = true;
        else
            howToPlay = false;

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        //transform.position = new Vector3(transform.position.x + 1.6f, transform.position.y, transform.position.z);

        gyroEnabled = EnableGyro();
        getAbsulutez = true;
        //absoluteZ = Gyro();

    }

    private void Update()
    {

        

        Gyro();

        if ((Input.GetKeyDown(KeyCode.LeftArrow) || pitch <= -lateralTilt[level]) && (Convert.ToBoolean(PlayerPrefs.GetString("LateralTilt"))  || howToPlay))
        {
            if (!left)
            {
                ChangeLane(1);
                left = true;
            }

            Debug.Log("Yaw lateraltilr right");
            //Debug.Log("Yaw Pitch= " + pitch + "  <=  " + -lateralTilt[level]);
            //if (!(transform.position.x >= m_TargetPosition.x))
            //transform.position += Vector3.right * Time.deltaTime * lerpTime;


        }
        else if ((Input.GetKeyDown(KeyCode.RightArrow) || pitch >= lateralTilt[level]) && (Convert.ToBoolean(PlayerPrefs.GetString("LateralTilt")) || howToPlay))
        {
            if (!right)
            {
                ChangeLane(-1);
                right = true;
            }

            Debug.Log("Yaw lateraltilr left");
            //Debug.Log("Yaw Pitch= " + pitch + "  >=  " + lateralTilt[level]);
            //if (!(transform.position.x <= m_TargetPosition.x))
            //transform.position += Vector3.left * Time.deltaTime * lerpTime;


        }
        else if ((Input.GetKeyDown(KeyCode.UpArrow) || roll >= extension[level]) && (Convert.ToBoolean(PlayerPrefs.GetString("Extension")) || howToPlay))
        {
            Debug.Log("Yaw jump");
            Jump();
        }
        else if ((Input.GetKeyDown(KeyCode.DownArrow) || roll <= flexsion[level]) && (Convert.ToBoolean(PlayerPrefs.GetString("Flexsion")) || howToPlay))
        {
            Debug.Log("Yaw sliding");
            if (!m_Sliding)
                Slide();
        }
        else if (yaw >= rotation[level] && (Convert.ToBoolean(PlayerPrefs.GetString("Rotation")) || howToPlay))
        {
            Debug.Log("Yaw leftrotation");
            //left rotation
            if (!leftRotation)
            {
                Rotation(1);
                leftRotation = true;
            }
            
        }
        else if (yaw <= -rotation[level] && (Convert.ToBoolean(PlayerPrefs.GetString("Rotation")) || howToPlay))
        {
            Debug.Log("Yaw rightrotation");
            //right rotation
            if (!rightRotation)
            {
                Rotation(-1);
                rightRotation = true;
            }
        }
        Debug.Log("Yaw ==================  " + yaw+ howToPlay);

        /*else if ((pitch >= 0 && pitch < 5) || (pitch < 0 && pitch > -5)) {

            if (left)
            {
                //ChangeLane(-1);

                //transform.position += Vector3.left * Time.deltaTime * lerpTime;

                //if (transform.position.x <= 0)
                    //left = false;
            }


            if (right)
            {
               // ChangeLane(1);

                //transform.position += Vector3.right * Time.deltaTime * lerpTime;

                //if (transform.position.x >= 0)
                    //right = false;
            }
                
        }*/



        // Vector3 verticalTargetPosition = m_TargetPosition;

        if (m_Sliding)
        {

            //StopSliding();
            //m_Sliding = false;
            StartCoroutine(NotSliding());
            // boxCollider.size = new Vector3 (boxCollider.size.x, 0.8f, boxCollider.size.z);
        }

        if (m_Jumping)
        {

            //anim.SetBool(s_JumpingHash, false);
            //m_Jumping = false;
            StartCoroutine(NotJumping());
           // boxCollider.size = new Vector3(boxCollider.size.x, 0.8f, boxCollider.size.z);

        }


    }

    IEnumerator NotJumping()
    {
        yield return new WaitForSeconds(2);
        m_Jumping = false;
        StopCoroutine(NotJumping());
    }

    IEnumerator NotSliding()
    {
        yield return new WaitForSeconds(2);
        m_Sliding = false;
        StopCoroutine(NotSliding());
    }

    public void Jump()
    {
        if (!m_Jumping)
        {
            if (m_Sliding)
                m_Sliding = false;

            anim.SetFloat(s_JumpingSpeedHash, 0.5f);
            anim.SetTrigger("Jump");
            playeranim.SetTrigger("JumpPlayer");
            m_Jumping = true;
            //boxCollider.center = new Vector3(boxCollider.center.x, 1f, boxCollider.center.z);
            //StartCoroutine(ColliderWait());
            
            Debug.Log("You must jump------------------------------");
        }
    }

    public void Slide()
    {
        if (!m_Sliding && !m_Jumping)
        {


            anim.SetFloat(s_JumpingSpeedHash, 0.5f);
            //anim.SetBool(s_SlidingHash, true);
            anim.SetTrigger("Slide");
            m_Sliding = true;
            boxCollider.center = new Vector3(boxCollider.center.x, 0.15f, boxCollider.center.z);
            StartCoroutine(ColliderWait());

        }
    }


    public void Rotation(int direction)
    {
        if (direction == 1)
        {
            playeranim.SetTrigger("LeftRotationPlayer");
            //left = false;
            StartCoroutine(NotLeftRotation());
        }
        else if (direction == -1)
        {
            playeranim.SetTrigger("RightRotationPlayer");
            //right = false;
            StartCoroutine(NotRightRotation());
        }
    }

    IEnumerator NotLeftRotation()
    {
        yield return new WaitForSeconds(1);
        leftRotation = false;
        StopCoroutine(NotLeftRotation());
    }

    IEnumerator NotRightRotation()
    {
        yield return new WaitForSeconds(1);
        rightRotation = false;
        StopCoroutine(NotRightRotation());
    }

    public void ChangeLane(int direction)
    {

        //int targetLane = m_CurrentLane + direction;

        //if (targetLane < 0)
        //{
            //targetLane = 0;
        //}
        //if (targetLane > 2)
        //{
            //targetLane = 2;
        //}

        // Ignore, we are on the borders.
        //return;

        Debug.Log("Try to change ========================");
        //m_CurrentLane = targetLane;
        //m_TargetPosition = new Vector3((m_CurrentLane - 1) * 1.5f, 0, transform.position.z);

        if (direction == 1)
        {
            playeranim.SetTrigger("LeftPlayer");
            //left = false;
            StartCoroutine(NotLeftSliding());
        }
        else if (direction == -1)
        {
            playeranim.SetTrigger("RightPlayer");
            //right = false;
            StartCoroutine(NotRightSliding());
        }

        //transform.position = Vector3.MoveTowards(transform.position, m_TargetPosition, laneChangeSpeed * Time.deltaTime * 15);
        //transform.position = Vector3.Lerp(transform.position, m_TargetPosition, Time.deltaTime * 15);
        //transform.position += Vector3.left * Time.deltaTime;

    }

    IEnumerator NotLeftSliding()
    {
        yield return new WaitForSeconds(2);
        left = false;
        StopCoroutine(NotLeftSliding());
    }

    IEnumerator NotRightSliding()
    {
        yield return new WaitForSeconds(2);
        right = false;
        StopCoroutine(NotRightSliding());
    }

    IEnumerator ColliderWait()
    {
        yield return new WaitForSeconds(2);
        //boxCollider.size = new Vector3(boxCollider.size.x, 0.8f, boxCollider.size.z);
        boxCollider.center = new Vector3(boxCollider.center.x, 0.6f, boxCollider.center.z);
        StopCoroutine(ColliderWait());

    }

    void OnTriggerEnter(Collider other)
    {
        CameraMove _cameraMove = cam.GetComponent<CameraMove> ();

        if (other.tag == "Obstacle")
        {
            if (numOfLife > 0)
            {
                gameController.GameOver();
                anim.SetTrigger("Hit");
                hit.Play();
                numOfLife--;
                PlayerPrefs.SetInt("numOfLife", numOfLife);
                return;
            }
                
            Debug.Log("Player Hit");
            anim.SetTrigger("Hit");
            anim.SetBool("Dead", true);
            gameController.GameOver();
            _cameraMove.StopMovement();
        }
        else if (other.tag == "Coin")
        {
            GameScore = GameScore + 1;
            scoreUI.text = GameScore  + "";
            coin.Play();
            //set on GUI
            Debug.Log("GameScore =  " + GameScore);
        }
        //gamecontroller.gameOver()
    }


    // Update is called once per frame
    void Gyro () {


        if (getAbsulutez)
        {
            //absoluteZ = yaw;
            getAbsulutez = false;
            absoluteZ = PlayerPrefs.GetFloat("absoluteZ");
            Debug.Log("absoluteZ==================  " + absoluteZ);
            //StartCoroutine(SetAbsuluteZ());
        }

        if (gyroEnabled)
        {
            x = Input.gyro.attitude.x;
            y = Input.gyro.attitude.y;
            z = Input.gyro.attitude.z;
            w = Input.gyro.attitude.w;

            // roll(x - axis rotation)
            float sinr = +2.0f * (w * x + y * z);
            float cosr = +1.0f - 2.0f * (x * x + y * y);
            roll = RadToDeg(Mathf.Atan2(sinr, cosr)) - 90;

            if (roll >= 180)
                roll = (360 - RadToDeg(Mathf.Atan2(sinr, cosr))) + 90;
            if (roll <= -180)
                roll = (360 + RadToDeg(Mathf.Atan2(sinr, cosr))) - 90;

            // pitch (y-axis rotation)
            float sinp = +2.0f * (w * y - z * x);
            pitch = RadToDeg(Mathf.Asin(sinp));

            // yaw (z-axis rotation)
            float siny = +2.0f * (w * z + x * y);
            float cosy = +1.0f - 2.0f * (y * y + z * z);
            yaw = RadToDeg(Mathf.Atan2(siny, cosy)) - absoluteZ;

            if (yaw >= 180)
                yaw = (360 - RadToDeg(Mathf.Atan2(siny, cosy))) + absoluteZ;
            if (yaw <= -180)
                yaw = (360 + RadToDeg(Mathf.Atan2(siny, cosy))) - absoluteZ;


            
        }

        //return yaw;

    }


    IEnumerator SetAbsuluteZ()
    {
        yield return new WaitForSeconds(3);

        x = Input.gyro.attitude.x;
        y = Input.gyro.attitude.y;
        z = Input.gyro.attitude.z;
        w = Input.gyro.attitude.w;

        float siny = +2.0f * (w * z + x * y);
        float cosy = +1.0f - 2.0f * (y * y + z * z);
        absoluteZ = RadToDeg(Mathf.Atan2(siny, cosy));
        Debug.Log("absoluteZ==================  " + absoluteZ);

        StopCoroutine(SetAbsuluteZ());

    }

    private float RadToDeg(float angle) {
        return angle * 180 / Mathf.PI;
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            Input.location.Start();
            return true;
        }
            return false;
    }
}
