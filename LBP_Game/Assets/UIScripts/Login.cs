﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;

public class Login : MonoBehaviour {

    public Canvas forgetPassword;
    public Canvas register;

    public Text Lemail;
    public Text Lpassword;
    public GameObject rememberMeCheck;

    public Text Remail;
    public Text Rusername;
    public Text Rpassword;
    public Text RconfPassword;

    public Text Femail;

    public GameObject erroMessage;
    public GameObject FerroMessage;
    public GameObject EmailSentMessage;
    public GameObject RerroMessage;
    public GameObject RidenticalPassword;
    public GameObject RcorrectEmailMsg;
    public GameObject RExistEmailMsg;

    //login data
    string m_Email;
    string m_Password;
    bool m_RememberMe;

    //register
    string m_Username;

    //settings


    // Use this for initialization
    void Start () {

        //UnityEngine.XR.XRSettings.enabled = false;

        //check if remember me >> main menu 
        if (PlayerPrefs.HasKey("RememberMe")) {

            m_RememberMe = Convert.ToBoolean(PlayerPrefs.GetString("RememberMe"));

            if (m_RememberMe)
                SceneManager.LoadScene("MainMenu");
        }

	}

    public void RememberMe() {

        m_RememberMe = rememberMeCheck.GetComponent<Toggle>().isOn;
        //Debug.Log("The M remember " + m_RememberMe);

    }

    public void GameLogin() {

        //Wrong username or password >> 
        //main menu Rebemember me?
        if (PlayerPrefs.HasKey("Email") && PlayerPrefs.HasKey("Password"))
        {
            if (PlayerPrefs.GetString("Email").Equals(Lemail.text) && PlayerPrefs.GetString("Password").Equals(Lpassword.text))
            {
                PlayerPrefs.SetString("RememberMe", m_RememberMe + "");
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                erroMessage.GetComponent<Text>().enabled = true;
            }
        }
        else {
            erroMessage.GetComponent<Text>().enabled = true;
        }

    }

    public void OpenForgetPasswordW() { forgetPassword.enabled = true; }

    public void CloseForgetPasswordW() {

        forgetPassword.enabled = false;
        FerroMessage.GetComponent<Text>().enabled = false;
        EmailSentMessage.GetComponent<Text>().enabled = false;
    }

    public void OpenRegisterW() { register.enabled = true; }

    public void CloseRegisterW() { register.enabled = false; }

    public void SendNewPassword() {

        //Wrong mail
        //Sent succsussfuly 
        string email;
        string password;
        if (PlayerPrefs.HasKey("Email"))
        {
            email = Femail.text;
            if (PlayerPrefs.GetString("Email").Equals(email)) {
                password = RandomPassword();
                SendMail(email, password);
                PlayerPrefs.SetString("Password", password);
                EmailSentMessage.GetComponent<Text>().enabled = true;
            }
            else
            {
                FerroMessage.GetComponent<Text>().enabled = true;
            }
        }
        else
        {
            FerroMessage.GetComponent<Text>().enabled = true;
        }

    }

    void SendMail(string email, string password) {

        MailMessage mail = new MailMessage();
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("LBPExerGame@gmail.com");
        mail.To.Add(email);
        mail.Subject = "New Password";
        mail.Body = "Your new password is: " + password;

        smtpServer.Port = 587;
        smtpServer.Credentials = new NetworkCredential("LBPExerGame@gmail.com", "lbpgame123") as System.Net.ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };


        smtpServer.Send(mail);
    }

    string RandomPassword() {

        string path = System.IO.Path.GetRandomFileName();
        path = path.Replace(".", ""); // Remove period.
        return path.Substring(0, 8);  // Return 8 character string
        
    }

    public void Register() {

        //Email already exists 
        //Required field 
        //Password length 
        //Succsussfully registered 
        RerroMessage.GetComponent<Text>().enabled = false;
        RExistEmailMsg.GetComponent<Text>().enabled = false;
        RcorrectEmailMsg.GetComponent<Text>().enabled = false;

        if (Remail.text.Equals("") || Rusername.text.Equals("") || Rpassword.text.Equals("") || RconfPassword.text.Equals("")) {

            RerroMessage.GetComponent<Text>().enabled = true;
            return; 
        }
        else RerroMessage.GetComponent<Text>().enabled = false;

        if (!Remail.text.Contains("@") && !Remail.text.Contains("."))
        {

            RcorrectEmailMsg.GetComponent<Text>().enabled = true;
            return;
        }

        if (PlayerPrefs.HasKey("Email") && PlayerPrefs.GetString("Email").Equals(Remail.text)) {

            RExistEmailMsg.GetComponent<Text>().enabled = true;
            return;
        }
        else RExistEmailMsg.GetComponent<Text>().enabled = false;

        if (!Rpassword.text.Equals(RconfPassword.text)) {

            RidenticalPassword.GetComponent<Text>().enabled = true;
            return;
        }


        Debug.Log("Password1= "+ Rpassword.text+"---------"+ RconfPassword.text + " Logic "+!Rpassword.text.Equals(RconfPassword.text));

        //if Succussfuly registered:
        //open main menu 
        SetSettings();
        CreateProfile();
        CreateReport();
        SceneManager.LoadScene("MainMenu");


    }

    private void CreateReport()
    {
        //string path = @"HistoryReport.txt";

        // Create a file to write to.

        //File f = new File(@"HistoryReport.txt");

        File.Create(Application.persistentDataPath + "HistoryReport.txt").Dispose();

        StreamWriter sw = new StreamWriter(Application.persistentDataPath + "HistoryReport.txt");


        //sw.Write("15/01/2018:1:1:5:0:false:true:false\n");
        //sw.Write("15/02/2018:1:1:5:0:true:true:false\n");
        //sw.Write("15/03/2018:1:1:5:0:true:true:true\n");

        sw.Close();
        
    }

    public void RemovePasswordErrorMsg()
    {
        if (Rpassword.Equals(RconfPassword)) {

            RidenticalPassword.GetComponent<Text>().enabled = false;

        }
    }

    public void RemoveEmailErrorMsg()
    {
        if (Remail.text.Contains("@") && Remail.text.Contains("."))
        {

            RcorrectEmailMsg.GetComponent<Text>().enabled = false;

        }
    }

    void SetSettings() {

        //Target + Current 

        PlayerPrefs.SetString("Flexsion", "True");
        PlayerPrefs.SetString("Extension", "True");
        PlayerPrefs.SetString("LateralTilt", "True");
        PlayerPrefs.SetString("Rotation", "True");

        PlayerPrefs.SetInt("Repetition", 3);
        PlayerPrefs.SetInt("Frequency", 1);
        PlayerPrefs.SetInt("Days", 1);
        PlayerPrefs.SetInt("Level", 0);

        PlayerPrefs.SetInt("TargetScore", 5);
        PlayerPrefs.SetInt("CurrentScore", 0);

        PlayerPrefs.SetInt("Evaluation", 0);


    }

    void CreateProfile() {

        //Set player information 
        PlayerPrefs.SetString("Email", Remail.text);
        PlayerPrefs.SetString("Username", Rusername.text);
        PlayerPrefs.SetString("Password", Rpassword.text);
        PlayerPrefs.SetString("RememberMe", rememberMeCheck.GetComponent<Toggle>().isOn+"");

    }
}
