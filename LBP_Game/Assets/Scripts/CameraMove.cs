﻿
using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

	public float moveSpeed;
	public GameObject mainCamera;
    public bool move = false;
    public GameObject Player;
    Animator anim;
    private GameController gameController;


    public bool moveleft = false;
    // Use this for initialization
    void Start () {

       // mainCamera.transform.localPosition = new Vector3 ( 0, 0, 0 );
		//mainCamera.transform.localRotation = Quaternion.Euler (18, 180, 0);

        moveSpeed = 7 + 2 * PlayerPrefs.GetInt("Level");

        anim = Player.GetComponent<Animator>();
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
	

	void FixedUpdate()
	{
        if (gameController.getGameStart())
        {
            anim.SetBool("Move", true);
            Debug.Log("Move is =============== true");
            move = true;
        }
            
        if (move)
		    MoveObj ();

        if (moveleft)
            MoveObjLeft();
	}

    void MoveObjLeft()
    {
        move = false;
        float moveAmount = Time.smoothDeltaTime * moveSpeed;
        transform.Translate(moveAmount, 0f, 0f);
    }

	void MoveObj() {
        moveleft = false;
		float moveAmount = Time.smoothDeltaTime * moveSpeed;
		transform.Translate ( 0f, 0f, moveAmount );	
	}

    public bool StopMovement() {

        Debug.Log("Trying to stop the camera");
        float timer = 0.0f;
        int seconds = 0;
        while (true) {

            timer += Time.deltaTime;
            seconds = (int) timer % 60;

            moveSpeed = moveSpeed - seconds;

            if (moveSpeed == 0)
            {
                return true;

            }
                

        }

    }


}























