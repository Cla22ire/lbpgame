﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {


    public float gazeTime = 2f;
    private float timer;

    private bool gazedAt;
    Transform child;
    Vector3 newScale;
    Vector3 newPosition;
    private GameController gameController;

    public float Wait;
    public Text countText;
    int count = 4;

    // Use this for initialization
    void Start () {

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        StartCoroutine(GameStartCount());
    }

    IEnumerator GameStartCount()
    {

        while (true)
        {
            
            count--;
            countText.text = count+"";

            if (count == -1) {
                PointerDown();
            }
            yield return new WaitForSeconds(Wait);
        }

    }

    // Update is called once per frame
    void Update () {

        if (gazedAt) {

            timer += Time.deltaTime;
            child = transform.GetChild(0);
            Vector3 childScale = new Vector3(timer/gazeTime, child.localScale.y, child.localScale.z);
            Vector3 childPosition = new Vector3(-0.5f + (timer/ gazeTime)/2 , child.localPosition.y, child.localPosition.z);

            child.localScale = childScale;
            child.localPosition = childPosition;

            if (timer >= gazeTime) {
                ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
                timer = 0f;
                //GetComponent<Collider>().enabled = false;
            }
        }
	}

    public void PointerEnter() {

        //gazedAt = true;

        //Debug.Log("Pointer has entered the cube and the game must start");
    }

    public void PointerExit() {

        gazedAt = false;
        timer = 0f;
        child = transform.GetChild(0);
        newScale = new Vector3(0.01f, 1f, 1f);
        newPosition = new Vector3(0.5f, 0f, 0.5f);
        child.localScale = newScale;
        child.localPosition = newPosition;
        //GetComponent<Collider>().enabled = false;
        //GetComponent<Collider>().enabled = true;
    }

    public void PointerDown() {

        gameController.setGameStart(true);
        Destroy(this.gameObject);
        //GetComponent<MeshCollider>().enabled = false;
    }

}
