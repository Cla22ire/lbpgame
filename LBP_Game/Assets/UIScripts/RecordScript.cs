﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordScript : MonoBehaviour {

    public Text date;
    public Text frequency;
    public Text repetition;
    public Text days;
    public Text programExer;
    public Text level;
    public Image freqFill;
    public Text freqPer;


}
