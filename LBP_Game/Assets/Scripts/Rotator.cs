﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    void Start()
    {
        transform.Rotate(new Vector3(90, 0, 0));
        //GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update () {

        transform.Rotate(new Vector3(0, 0, 45) * 2* Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            //GetComponent<AudioSource>().Play();
            Destroy(gameObject);
            //StartCoroutine(DestroyCoin());
        }

        //gamecontroller.gameOver()
    }


}
