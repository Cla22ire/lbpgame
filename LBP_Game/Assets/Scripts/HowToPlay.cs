﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HowToPlay : MonoBehaviour {

    public float Wait;
    public Canvas[] exercises;

    private int count;

    public Image exitImg;
    private bool exit;
    
    private int [] enabledExercises;

    // Use this for initialization
    void Start () {

        count = 0;
        FillExerciseList();
        StartCoroutine(Instructions());
	}

    IEnumerator Instructions()
    {

        while (true) {

            for (int i = 0; i < exercises.Length; i++)
            {
                exercises[i].enabled =false;
            }

            if (enabledExercises[count] == -1) {
                count++;
                if (count == exercises.Length)
                {
                    count = 0;
                }
                continue;
            }
                

            exercises[count].enabled = true;
            count++;
            if (count == exercises.Length)
            {
                count = 0;
            }


            yield return new WaitForSeconds(Wait);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (exit == true)
        {
            //Reduce fill amount over 30 seconds
            exitImg.fillAmount = exitImg.fillAmount + (0.2f * Time.deltaTime);
            if (exitImg.fillAmount == 1)
            {
                SceneManager.LoadScene("Game");
                Debug.Log("Now you can load the game");
            }
        }

    }

    private void FillExerciseList()
    {
        List<int> exerList = new List<int>();

        if (Convert.ToBoolean(PlayerPrefs.GetString("Extension")))
        {
            exerList.Add(0);
        }
        else exerList.Add(-1);

        if (Convert.ToBoolean(PlayerPrefs.GetString("Flexsion")))
        {
            exerList.Add(1);
        }
        else exerList.Add(-1);

        if (Convert.ToBoolean(PlayerPrefs.GetString("LateralTilt")))
        {
            exerList.Add(2);
        }
        else exerList.Add(-1);

        if (Convert.ToBoolean(PlayerPrefs.GetString("Rotation")))
        {
            exerList.Add(3);
        }
        else exerList.Add(-1);

        enabledExercises = exerList.ToArray();
        //PlayerPrefs.SetInt("Repetition", 5);
    }

    public void PointerEnter()
    {

        exit = true;

    }

    public void PointerExit()
    {

        exit = false;
        exitImg.fillAmount = 0f;

    }


}
