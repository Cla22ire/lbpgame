﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class Calibration : MonoBehaviour {



    public Image calibrationImg;
    public bool calibration;

    private Gyroscope gyro;
    private bool gyroEnabled;
    float x, y, z, w;
    float roll, pitch, yaw, absoluteZ;

    //public float waitTime = 30.0f;
    //public Camera GVrCamera;
    //public Vector3 GVRPosition;


    // Use this for initialization
    void Start () {

        StartCoroutine(LoadDevice("cardboard"));

        calibration = false;
        gyroEnabled = EnableGyro();
        //Instantiate(GVrCamera, new Vector3(0,0,0) , Quaternion.identity);
        //cooldown.
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (String.Compare(XRSettings.loadedDeviceName, newDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(newDevice);
            yield return null;
            XRSettings.enabled = true;
        }
    }

    // Update is called once per frame
    void Update () {

        Gyro();

        if (calibration == true)
        {
            //Reduce fill amount over 30 seconds
            calibrationImg.fillAmount = calibrationImg.fillAmount + (0.2f * Time.deltaTime);
            if (calibrationImg.fillAmount == 1)
            {
                x = Input.gyro.attitude.x;
                y = Input.gyro.attitude.y;
                z = Input.gyro.attitude.z;
                w = Input.gyro.attitude.w;

                float siny = +2.0f * (w * z + x * y);
                float cosy = +1.0f - 2.0f * (y * y + z * z);
                absoluteZ = RadToDeg(Mathf.Atan2(siny, cosy));
                PlayerPrefs.SetFloat("absoluteZ", absoluteZ);

                Debug.Log("absoluteZ==================  " + absoluteZ);


                SceneManager.LoadScene("HowToPlay");
                Debug.Log("Now you can load the game");
            }
        }

    }


    public void PointerEnterCalibration()
    {

        calibration = true;

    }

    public void PointerExitCalibration()
    {

        calibration = false;
        calibrationImg.fillAmount = 0f;

    }

    void Gyro()
    {



        if (gyroEnabled)
        {
            x = Input.gyro.attitude.x;
            y = Input.gyro.attitude.y;
            z = Input.gyro.attitude.z;
            w = Input.gyro.attitude.w;

            // roll(x - axis rotation)
            float sinr = +2.0f * (w * x + y * z);
            float cosr = +1.0f - 2.0f * (x * x + y * y);
            roll = RadToDeg(Mathf.Atan2(sinr, cosr)) - 90;

            if (roll >= 180)
                roll = (360 - RadToDeg(Mathf.Atan2(sinr, cosr))) + 90;
            if (roll <= -180)
                roll = (360 + RadToDeg(Mathf.Atan2(sinr, cosr))) - 90;

            // pitch (y-axis rotation)
            float sinp = +2.0f * (w * y - z * x);
            pitch = RadToDeg(Mathf.Asin(sinp));

            // yaw (z-axis rotation)
            float siny = +2.0f * (w * z + x * y);
            float cosy = +1.0f - 2.0f * (y * y + z * z);
            yaw = RadToDeg(Mathf.Atan2(siny, cosy)) - absoluteZ;

            if (yaw >= 180)
                yaw = (360 - RadToDeg(Mathf.Atan2(siny, cosy))) + absoluteZ;
            if (yaw <= -180)
                yaw = (360 + RadToDeg(Mathf.Atan2(siny, cosy))) - absoluteZ;


        }

        //return yaw;

    }


    private float RadToDeg(float angle)
    {
        return angle * 180 / Mathf.PI;
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            Input.location.Start();
            return true;
        }
        return false;
    }


}
