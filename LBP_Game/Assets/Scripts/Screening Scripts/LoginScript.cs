﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginScript : MonoBehaviour {

	private FirebaseAuth auth;
    public InputField email, password;
    public Button LoginButton;
    public Text ErrorText;

	
	 void Start()
    {
        auth = FirebaseAuth.DefaultInstance;
        //Just an example to save typing in the login form
      //  UserNameInput.text = "demofirebase@gmail.com";
      //  PasswordInput.text = "abcdefgh";

        LoginButton.onClick.AddListener(() => Login(email.text, password.text));
    }

	  private void UpdateErrorMessage(string message)
	  {
       ErrorText.text = message;
       Invoke("ClearErrorMessage", 3);  }

       void ClearErrorMessage()
	    {
			 ErrorText.text = "";
		}


	public void Login(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync error: " + task.Exception);
                if (task.Exception.InnerExceptions.Count > 0)
                    UpdateErrorMessage(task.Exception.InnerExceptions[0].Message);
                return;
            }

            FirebaseUser user = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                user.DisplayName, user.UserId);

            PlayerPrefs.SetString("LoginUser", user != null ? user.Email : "Unknown");
            Application.LoadLevel("Screening");
        });
		
	}

	

}
