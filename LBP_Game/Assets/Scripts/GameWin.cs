﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class GameWin : MonoBehaviour {

    public Image replay;
    public bool replayerSelected;

    public Image mainMenuimg;
    public bool mainMenu;


    public Text score;
    public Image star1;
    public Image star2;
    public Image star3;

    int currentScore;
    int targetScore;
    int numOfHit;

    bool fillStar1;
    int fillvalue;

    public Canvas levelUp;

    // Use this for initialization
    void Start () {

        //DeletThiFunction();
        mainMenu = false;
        replayerSelected = false;

        StartCoroutine(LoadDevice("cardboard"));

        SaveToFile();
        
        currentScore = PlayerPrefs.GetInt("CurrentScore") + PlayerPrefs.GetInt("Repetition");
        targetScore = PlayerPrefs.GetInt("TargetScore");
        numOfHit = 3 - PlayerPrefs.GetInt("numOfLife");

        if (!PlayerPrefs.HasKey("numOfLife"))
            numOfHit = 3;

        UpdateSettings();

        score.text = (currentScore - numOfHit )+ "";
        FillStars();
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (String.Compare(XRSettings.loadedDeviceName, newDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(newDevice);
            yield return null;
            XRSettings.enabled = true;
        }
    }

    private void FillStars()
    {
        float fill = ((currentScore - numOfHit) * 1.0f / currentScore * 1.0f) * 100f;
        //float fill = (PlayerPrefs.GetInt("numOfLife") * 1.0f / PlayerPrefs.GetInt("Repetition") * 1.0f) * 100f;
        fillvalue = (int) fill;
        //Debug.Log("=====================================  " + fill +"   " + fillvalue);
        fillStar1 = true;
    }

    private void DeletThiFunction()
    {
        /*PlayerPrefs.SetString("Flexsion", "true");
        PlayerPrefs.SetString("Extension", "true");
        PlayerPrefs.SetString("LateralTilt", "false");

        PlayerPrefs.SetInt("Repetition", 5);
        PlayerPrefs.SetInt("Frequency", 1);
        PlayerPrefs.SetInt("Days", 1);
        PlayerPrefs.SetInt("Level", 0);

        PlayerPrefs.SetInt("TargetScore", 16);
        PlayerPrefs.SetInt("CurrentScore", 3);*/

        if (PlayerPrefs.GetInt("Evaluation") == 0)
        {

            PlayerPrefs.SetInt("Evaluation", 1);

            PlayerPrefs.SetString("Flexsion", "false");
            PlayerPrefs.SetString("Extension", "false");
            PlayerPrefs.SetString("LateralTilt", "true");
            PlayerPrefs.SetString("Rotation", "false");
        }
        else if (PlayerPrefs.GetInt("Evaluation") == 1)
        {
            PlayerPrefs.SetInt("Evaluation", 0);

            PlayerPrefs.SetString("Flexsion", "false");
            PlayerPrefs.SetString("Extension", "false");
            PlayerPrefs.SetString("LateralTilt", "false");
            PlayerPrefs.SetString("Rotation", "true");
        }
    }

    private void SaveToFile()
    {
        List<string> temp = new List<string>();

        DateTime dateTime = DateTime.UtcNow.Date;
        string date = dateTime.ToString("dd/MM/yyy");
        string record= date + ":" + PlayerPrefs.GetInt("Frequency") + ":" + PlayerPrefs.GetInt("Days") + ":" + PlayerPrefs.GetInt("Repetition") + ":" + PlayerPrefs.GetInt("Level") + 
            ":" + PlayerPrefs.GetString("Flexsion") + ":" + PlayerPrefs.GetString("Extension") + ":" + PlayerPrefs.GetString("LateralTilt")+ ":" + PlayerPrefs.GetString("Rotation")  + "\n";

        //Debug.Log("Save Format--------------------------------------" + record);

        StreamReader reader = new StreamReader(Application.persistentDataPath + "HistoryReport.txt");
        string line = reader.ReadLine();
        while (line != null)
        {
            temp.Add(line);
            Debug.Log(line);
            line = reader.ReadLine();
        }

        reader.Close();

        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "HistoryReport.txt");
        foreach (string rec in temp) {
            writer.Write(rec+"\n");
        }

        writer.Write(record);
        writer.Close();

    }

    private void UpdateSettings()
    {
        if (currentScore == PlayerPrefs.GetInt("TargetScore"))
        {
            int level = PlayerPrefs.GetInt("Level");
            if (level < 2)
            {
                level = level + 1;
                levelUp.enabled = true;
                PlayerPrefs.SetInt("Level", level);
                PlayerPrefs.SetString("LevelUp", "true");
                
            }
            PlayerPrefs.SetInt("CurrentScore", 0);
        }
        else {
            PlayerPrefs.SetInt("CurrentScore", currentScore);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (replayerSelected == true)
        {
            //Reduce fill amount over 30 seconds
            replay.fillAmount = replay.fillAmount + (0.2f * Time.deltaTime);
            if (replay.fillAmount == 1)
            {
                SceneManager.LoadScene("BeforeGame");
                Debug.Log("Now you can load the game");
            }
        }

        if (mainMenu == true)
        {
            //Reduce fill amount over 30 seconds
            mainMenuimg.fillAmount = mainMenuimg.fillAmount + (0.2f * Time.deltaTime);
            if (mainMenuimg.fillAmount == 1)
            {
                SceneManager.LoadScene("MainMenu");
                Debug.Log("Now you can load the MainMenu");
            }
        }

        if (fillvalue <= 33 && fillStar1) {
            star1.fillAmount = star1.fillAmount + (0.1f * Time.deltaTime * 9);

            if (star1.fillAmount * 100 >= (fillvalue *1.0f/33 * 100))
            {
                fillStar1 = false;
            }

        }
        else if (fillvalue >= 33 && fillvalue <= 66 && fillStar1) {
            star1.fillAmount = star1.fillAmount + (0.1f * Time.deltaTime * 9);

            if (star1.fillAmount == 1)
            {
                star2.fillAmount = star2.fillAmount + (0.1f * Time.deltaTime * 9);

                if (star2.fillAmount * 100 >= ((fillvalue - 33) * 1.0f / 33 * 100))
                {
                    fillStar1 = false;
                }
            }

        }
        else if (fillvalue >= 66 && fillStar1) {
            star1.fillAmount = star1.fillAmount + (0.1f * Time.deltaTime * 9);

            if (star1.fillAmount == 1)
            {
                star2.fillAmount = star2.fillAmount + (0.1f * Time.deltaTime * 9);

                if (star2.fillAmount == 1)
                {
                    star3.fillAmount = star3.fillAmount + (0.1f * Time.deltaTime * 9);

                    if (star3.fillAmount * 100 >= ((fillvalue - 66) * 1.0f / 33 * 100))
                    {
                        fillStar1 = false;
                    }
                }
            }

        }

    }

    public void PointerEnter()
    {

        replayerSelected = true;

    }

    public void PointerExit()
    {

        replayerSelected = false;
        replay.fillAmount = 0f;

    }

    public void PointerEnterMainMenu()
    {

        mainMenu = true;

    }

    public void PointerExitMainMenu()
    {

        mainMenu = false;
        mainMenuimg.fillAmount = 0f;

    }

}
