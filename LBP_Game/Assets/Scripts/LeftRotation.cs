﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRotation : MonoBehaviour {

    public Transform cam;
    public Transform rabbit;
    public Transform gamecontroller;
    public Transform boundry;

    private Gyroscope gyro;
    private bool gyroEnabled;
    float x, y, z, w;
    float roll, pitch, yaw, absoluteZ;
    bool left = false, right = false;

    // Use this for initialization
    void Start () {

        cam.Rotate(new Vector3(0, -90, 0));
        rabbit.Rotate(new Vector3(0, -90, 0));
        gamecontroller.Rotate(new Vector3(0, -90, 0));
        boundry.Rotate(new Vector3(0, -90, 0));


        gyroEnabled = EnableGyro();

        x = Input.gyro.attitude.x;
        y = Input.gyro.attitude.y;
        z = Input.gyro.attitude.z;
        w = Input.gyro.attitude.w;

        float siny = +2.0f * (w * z + x * y);
        float cosy = +1.0f - 2.0f * (y * y + z * z);
        absoluteZ = RadToDeg(Mathf.Atan2(siny, cosy));
    }
	
	// Update is called once per frame
	void Update () {
        Gyro();

    }

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {

            cam.Rotate(new Vector3(0, -90, 0));
            rabbit.Rotate(new Vector3(0, -90, 0));
            gamecontroller.Rotate(new Vector3(0, -90, 0));
            boundry.Rotate(new Vector3(0, -90, 0));

            /*if (yaw >= 15)
            {
                cam.Rotate(new Vector3(0, -90, 0));
                rabbit.Rotate(new Vector3(0, -90, 0));
                gamecontroller.Rotate(new Vector3(0, -90, 0));
                boundry.Rotate(new Vector3(0, 90, 0));
            }
            else if (yaw <= -15)
            {
                cam.Rotate(new Vector3(0, 0, 0));
                rabbit.Rotate(new Vector3(0, 0, 0));
                gamecontroller.Rotate(new Vector3(0, 0, 0));
                boundry.Rotate(new Vector3(0, 180, 0));
            }
            else 
            {
                gameController.GameOver();
                gameController.GameOver();
                gameController.GameOver();
                gameController.GameOver();
            }*/

        }

    }


    void Gyro()
    {



        if (gyroEnabled)
        {
            x = Input.gyro.attitude.x;
            y = Input.gyro.attitude.y;
            z = Input.gyro.attitude.z;
            w = Input.gyro.attitude.w;

            // roll(x - axis rotation)
            float sinr = +2.0f * (w * x + y * z);
            float cosr = +1.0f - 2.0f * (x * x + y * y);
            roll = RadToDeg(Mathf.Atan2(sinr, cosr)) - 90;

            if (roll >= 180)
                roll = (360 - RadToDeg(Mathf.Atan2(sinr, cosr))) + 90;
            if (roll <= -180)
                roll = (360 + RadToDeg(Mathf.Atan2(sinr, cosr))) - 90;

            // pitch (y-axis rotation)
            float sinp = +2.0f * (w * y - z * x);
            pitch = RadToDeg(Mathf.Asin(sinp));

            // yaw (z-axis rotation)
            float siny = +2.0f * (w * z + x * y);
            float cosy = +1.0f - 2.0f * (y * y + z * z);
            yaw = RadToDeg(Mathf.Atan2(siny, cosy)) - absoluteZ;

            if (yaw >= 180)
                yaw = (360 - RadToDeg(Mathf.Atan2(siny, cosy))) + absoluteZ;
            if (yaw <= -180)
                yaw = (360 + RadToDeg(Mathf.Atan2(siny, cosy))) - absoluteZ;


        }

    }



    private float RadToDeg(float angle)
    {
        return angle * 180 / Mathf.PI;
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            Input.location.Start();
            return true;
        }
        return false;
    }

}
