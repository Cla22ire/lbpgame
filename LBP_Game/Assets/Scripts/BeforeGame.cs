﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class BeforeGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadDevice("cardboard"));
        
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (String.Compare(XRSettings.loadedDeviceName, newDevice, true) != 0)
        {
            XRSettings.LoadDeviceByName(newDevice);
            yield return null;
            XRSettings.enabled = true;
        }

        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Calibration");
       
    }

}
